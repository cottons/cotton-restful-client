package cotton.restful;

import cotton.restful.core.exceptions.RestfulFailure;

import java.net.URL;
import java.util.Map;

public interface RestfulClient {
    Response get(URL url, String resource) throws RestfulFailure;
    boolean getAndCheck(URL url, String resource) throws RestfulFailure;
    Response post(URL url, String resource) throws RestfulFailure;
    Response post(URL url, String resource, Map<String, Object> parameters) throws RestfulFailure;

    RestfulSecureConnection secureConnection(URL url, URL certificate, String password);

    interface RestfulSecureConnection {
        Response post(String resource) throws RestfulFailure;
        Response post(String resource, Map<String, Object> parameters) throws RestfulFailure;
    }

    interface Response {
        String content();
    }
}
