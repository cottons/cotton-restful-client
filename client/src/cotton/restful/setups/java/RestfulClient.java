package cotton.restful.setups.java;

import cotton.restful.core.exceptions.RestfulFailure;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

public class RestfulClient implements cotton.restful.RestfulClient {

    @Override
    public Response get(URL url, String resource) throws RestfulFailure {
        try {
            HttpGet get = new HttpGet(resourceUrl(url, resource));
            HttpResponse response = client().execute(get);
            Response result = responseOf(response);

            if (response.getStatusLine().getStatusCode() == 501)
                throw new RestfulFailure(result.content());

            return result;
        } catch (IOException exception) {
            throw new RestfulFailure(exception.getMessage());
        }
    }

    @Override
    public boolean getAndCheck(URL url, String resource) throws RestfulFailure {
        Response response = get(url, resource);
        return Boolean.valueOf(response.content());
    }

    @Override
    public Response post(URL url, String resource) throws RestfulFailure {
        return post(url, resource, Collections.emptyMap());
    }

    @Override
    public Response post(URL url, String resource, Map<String, Object> parameters) throws RestfulFailure {
        try {
            HttpPost post = new HttpPost(resourceUrl(url, resource));
            MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
            addParameters(entityBuilder, parameters);
            HttpResponse response = client().execute(post);
            return responseOf(response);
        } catch (Exception exception) {
            throw new RestfulFailure(exception.getMessage());
        }
    }

    @Override
    public RestfulSecureConnection secureConnection(URL url, URL certificate, String password) {
        return new RestfulSecureConnection() {
            @Override
            public Response post(String resource) throws RestfulFailure {
                return post(resource, Collections.emptyMap());
            }

            @Override
            public Response post(String resource, Map<String, Object> parameters) throws RestfulFailure {
                String resourceUrl = resourceUrl(url, resource);
                HttpPost post = new HttpPost(resourceUrl);
                MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
                HttpResponse response;

                addParameters(entityBuilder, parameters);
                addSecureParameters(entityBuilder, parameters);

                try {
                    post.setEntity(entityBuilder.build());
                    response = client().execute(post);
                } catch (IOException exception) {
                    throw new RestfulFailure(exception.getMessage());
                }

                int status = response.getStatusLine().getStatusCode();
                if (status < 200 || status >= 300)
                    throw new RestfulFailure(String.format("%s => %d - %s", url, status, response.getStatusLine().getReasonPhrase()));

                return responseOf(response);
            }

            private void addSecureParameters(MultipartEntityBuilder entityBuilder, Map<String, Object> parameters) throws RestfulFailure {
                if (certificate == null)
                    return;

                try {
                    Signer signer = new Signer();
                    long timestamp = new Date().getTime();
                    String hash = signer.hash(parameters, timestamp);

                    addSecureParameter(entityBuilder, "timestamp", String.valueOf(timestamp));
                    addSecureParameter(entityBuilder, "hash", hash);
                    addSecureParameter(entityBuilder, "signature", signer.sign(hash, certificate, password));
                } catch (Exception exception) {
                    throw new RestfulFailure(String.format("Could not sign with certificate: %s", certificate.toString()));
                }
            }
        };
    }

    private void addParameters(MultipartEntityBuilder builder, Map<String, Object> parameters) throws RestfulFailure {
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

        if (parameters.entrySet().size() <= 0)
            builder.addPart("mockParam", stringBodyOf("mock"));

        for (Map.Entry<String, Object> parameter : parameters.entrySet())
            builder.addPart(parameter.getKey(), stringBodyOf(parameter.getValue()));
    }

    private void addSecureParameter(MultipartEntityBuilder entityBuilder, String name, String value) throws RestfulFailure {
        entityBuilder.addPart(name, stringBodyOf(value));
    }

    private String resourceUrl(URL url, String resource) {
        return url.toString() + resource;
    }

    private Response responseOf(HttpResponse response) {
        return new Response() {

            @Override
            public String content() {
                try {
                    if (response == null)
                        return null;

                    return stringContentOf(response.getEntity().getContent());
                } catch (IOException e) {
                    return null;
                }
            }

            private String stringContentOf(InputStream input) {
                BufferedReader buffer = null;
                StringBuilder sb = new StringBuilder();
                String line;

                try {
                    buffer = new BufferedReader(new InputStreamReader(input));
                    while ((line = buffer.readLine()) != null) sb.append(line);
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (buffer != null) {
                        try {
                            buffer.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                return sb.toString();
            }
        };
    }

    private StringBody stringBodyOf(Object value) throws RestfulFailure {
        if (!(value instanceof String)) throw new RestfulFailure("value is not a string type");
        return new StringBody((String) value, ContentType.TEXT_PLAIN);
    }

    private HttpClient client() {
        return HttpClientBuilder.create().build();
    }

}
